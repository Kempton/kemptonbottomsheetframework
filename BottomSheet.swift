//
//  BottomSheet.swift
//  BottomSheetFramework
//
//  Created by Justin Kempton on 9/18/20.
//

import UIKit

/// The state the BottomSheet is in when not being manipulated
public enum BottomSheetState {
  /// The BottomSheet is not visible and is off the screen
  case dismissed
  /// The BottomSheet is in it's half way open state
  case collapsed
  /// The BottomSheet is fully visible
  case extended
}

/// The methods declared by the BottomSheetDelegate protocol allow the adopting delegate to respond to messages from the BottomSheet class
public protocol BottomSheetDelegate: AnyObject {
/**
      Relays the current state of the BottomSheet

       - Parameters:
          - state: The current state of the BottomSheet
*/
  func currentState(_ state: BottomSheetState)
}


/// A sheet that is displayed from the bottom of the screen that is attached to the top most window
public class BottomSheet: UIView {
  
  /// An optional title
  private var title: String? = nil
  /// An optional subtitle
  private var subTitle: String? = nil
  
  //UI
  /// The visual sheet that displays from the bottom of the screen
  private var sheet: UIView? = nil
  /// An optional dismiss button that is used to dismiss the BottomSheet
  private var dismissButton: UIButton? = nil
  /// A view header containing the title, subtitle and dismiss button
  private let header = UIView(frame: .zero)
  /// A container to holds the title and subtitle, if the optional title or subtitle are not provided, this container contracts and reconfigures
  private let labelsContainer = UILabel(frame: .zero)
  /// The label that displays the optional title if provided
  private let titleLabel = UILabel(frame: .zero)
  /// The label that displays the optional subtitle if provided
  private let subTitleLabel = UILabel(frame: .zero)
  /// A ScrollView that contains any custom content to be added
  private let scrollView = UIScrollView(frame: .zero)
  /// A container placed inside the ScrollView that holds the custom content
  public let contentContainer = UIView(frame: .zero)
  /// The background view that contains a gradient that can be customized
  private let background = BackgroundView()
  
  /**
   The delegate of the BottomSheet
 
   # Text
   The delegate must adopt the BottomSheetDelegate protocol. The BottomSheet class, which does not retain the delegate, invokes each protocol method the delegate implements.
   */
  public weak var delegate: BottomSheetDelegate?
  
  /// The maximum width of the BottomSheet
  private let maximumSheetWidth = CGFloat(500)
  /// An optional closure that handles the dismiss action
  private var dismissButtonTapCompletion: (() -> ())?
  /// The current state of the BottomSheet, defaults to dismissed
  private var state: BottomSheetState = .dismissed
  /// The Y location of the BottomSheet when it is fully visible, is recalculated on initialization and rotation
  private var locationExtended = CGFloat(0)
  /// The Y location of the BottomSheet when it is half way open, is recalculated on initialization and rotation
  private var locationCollapsed = CGFloat(0)
  /// The Y location of the BottomSheet when it is off screen and hidden, is recalculated on initialization and rotation
  private var locationDismissed = CGFloat(0)
  /// The height of the BottomSheet view, is recalculated on initialization and rotation
  private var sheetHeight = CGFloat(0)
  /// The height of the top window view, is recalculated on initialization and rotation
  private var topWindowHeight = CGFloat(0)
  /// The BottomSheets center point when panning begins
  private var sheetLastLocation: CGPoint = .zero
  /// The UIViewPropertyAnimator that the BottomSheet uses when animating into place
  private var animator: UIViewPropertyAnimator!
  /// A bottom constraint that the ScrollView changes depending on it's current state
  private var scrollViewBottomConstraint: NSLayoutConstraint!
  /// A constant that is configured to display the correct location for the scrollViewBottomConstraint
  private var scrollViewBottomConstraintConstant: CGFloat {
    return state == .extended ? 0 : -locationCollapsed + locationExtended
  }
  /// When enabled, this will allow the custom content provided in the ScrollView to scroll horizontally if it can.  If set to false, horizontal scrolling is disabled and will prevent custom content from trying to scroll horizontally.
  public var isHorizontalScrollingEnabled = true
  
  /**
   Initializes a new BottomSheet with
  
   - Parameters:
       - title: An optional title
       - subTitle:  An optional subtitle
       - gradient:  An optional gradient
       - dismissButtonTapCompletion: An optional dismiss button closure
   
   # Text
   A BottomSheet can be initialized with all or no parameters, allowing for lots of customization.
   */
  public required init(title: String? = nil,
                       subTitle: String? = nil,
                       gradient: (colorOne: UIColor, colorTwo: UIColor)? = nil,
                       dismissButtonTapCompletion: (() -> ())? = nil) {
    self.title = title
    self.subTitle = subTitle
    self.dismissButtonTapCompletion = dismissButtonTapCompletion
    super.init(frame: CGRect.zero)
    commonInit(gradient: gradient)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Public Methods
  
  /// Presents the BottomSheet in the top window
  public func presentBottomSheet() {
    //If this method is tried to be called again it will return early and prevent multiple UI sheets being initialized
    if sheet != nil { return }
    sheet = UIView(frame: .zero)
    guard let sheet = sheet, let topWindow = UIApplication.shared.windows.first!.rootViewController?.view else { return }
    
    //Get all the locations the BottomSheet will need to display it's states properly
    calculateSheetLocations(addedTo: topWindow)
    //Add all the UI needed to make a BottomSheet
    configureUIFrom(sheet, addedTo: topWindow)
    //Add a dismiss button if it was supplied in the initializer
    configureDismissButtonIfNeeded()
    
    //On first appearance, animate the BottomSheet to it's collapsed state
    state = .collapsed
    //notified the delegate of it's current state
    delegate?.currentState(state)
    //It was decided to use frames as the main container of the BottomSheet.  This allowed for the easier interuption during a pan gesture of the BottomSheet if an animation was taking place.
    //Animating constraints with interrupting pan gestures while the UIView.animate is happening is pretty difficult.  If the animation was always being allowed to complete and not be
    //interrupted, animating constraints could be a better alternative.  However, everything else uses constraints for the subviews of the BottomSheet.
    animator = UIViewPropertyAnimator(duration: 0.75, dampingRatio: 0.5) {
      var newFrame = sheet.frame
      newFrame.origin.y = self.locationCollapsed
      sheet.frame = newFrame
    }
    animator.startAnimation()
  }
  
  ///Dismisses the BottomSheet with an animator.  On completion, the sheet, and animator nil themselves out.  The sheet will remove it's self from self.
  public func dismissBottomSheet() {
    guard let sheet = sheet else { return }
    var newFrame = sheet.frame
    newFrame.origin.y = locationDismissed
    state = .dismissed
    delegate?.currentState(state)
    animator = UIViewPropertyAnimator(duration: 0.15, curve: .easeOut, animations: {
      sheet.frame = newFrame
    })
    animator.addCompletion { [weak self] _ in
      guard let self = self else { return }
      self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
      self.sheet?.removeFromSuperview()
      self.sheet = nil
      self.animator = nil
      self.removeFromSuperview()
    }
    animator.startAnimation()
  }
  
  // MARK: - Private Methods
  
  /**
      A method called on every intilization of a new BottomSheet.

       - Parameters:
          - gradient: The optional gradient supplied from the initializer
  */
  private func commonInit(gradient: (colorOne: UIColor, colorTwo: UIColor)? = nil) {
    //If custom gradients were supplied in the initializer, they will be applied here to the background view of the BottomSheet
    if let gradient = gradient {
      background.colorOne = gradient.colorOne
      background.colorTwo = gradient.colorTwo
      background.setNeedsDisplay()
    }
    //Add a notification to watch for device orientation changes
    NotificationCenter.default.addObserver(self, selector: #selector(BottomSheet.orientationChanged), name: UIDevice.orientationDidChangeNotification, object: nil)
  }
  
  /**
      Calculates all the locations the BottomSheet that are needed to display it's states properly

       - Parameters:
          - topWindow: The top window that contains the BottomSheet
  */
  private func calculateSheetLocations(addedTo topWindow: UIView) {
    topWindowHeight = topWindow.frame.height
    //sheet height is 85% of top window
    sheetHeight = topWindowHeight * 0.85
    locationDismissed = topWindowHeight
    locationExtended = topWindowHeight - sheetHeight
    locationCollapsed = (locationExtended + locationDismissed) / 2
  }
  
  /**
      Configures all the UI needed to properly display the BottomSheet

       - Parameters:
          - sheet: The single main sheet used to contain all the BottomSheet UI
          - topWindow: The top window that contains the BottomSheet
  */
  private func configureUIFrom(_ sheet: UIView, addedTo topWindow: UIView) {
    let sheetWidth = min(topWindow.frame.width, maximumSheetWidth)
    let sheetLocationX = topWindow.frame.width / 2 - sheetWidth / 2
    sheet.frame = CGRect(x:sheetLocationX, y: locationDismissed, width: sheetWidth, height: sheetHeight)
    sheet.addGestureRecognizer(UIPanGestureRecognizer(target:self, action:#selector(BottomSheet.pan)))
    topWindow.addSubview(sheet)
    
    background.translatesAutoresizingMaskIntoConstraints = false
    background.clipsToBounds = true
    background.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: 20.0)
    sheet.addSubview(background)
    
    header.translatesAutoresizingMaskIntoConstraints = false
    header.backgroundColor = .white
    header.roundCorners([.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: 20.0)
    sheet.addSubview(header)
    
    labelsContainer.translatesAutoresizingMaskIntoConstraints = false
    header.addSubview(labelsContainer)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = .systemFont(ofSize: 20, weight: .medium)
    titleLabel.text = title
    titleLabel.textAlignment = .center
    labelsContainer.addSubview(titleLabel)
    
    subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
    subTitleLabel.font = .systemFont(ofSize: 16, weight: .regular)
    subTitleLabel.text = subTitle
    subTitleLabel.textAlignment = .center
    subTitleLabel.textColor = .darkGray
    labelsContainer.addSubview(subTitleLabel)
    
    //Recalculate the margin between the title and subtitle, depending on the customization provided in the initializer
    let titleGap: CGFloat = title != nil && subTitle == nil || title == nil && subTitle != nil ? 0 : 3
    
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.contentInsetAdjustmentBehavior = .never
    let contentGuide = scrollView.contentLayoutGuide
    sheet.addSubview(scrollView)
    
    contentContainer.translatesAutoresizingMaskIntoConstraints = false
    scrollView.addSubview(contentContainer)
    
    //Since iOS 11 ScrollViews that use AutoContraints use FrameLayoutGuide and ContentLayoutGuide to properly layout their content
    let frameGuide = scrollView.frameLayoutGuide
    if !isHorizontalScrollingEnabled {
      NSLayoutConstraint.activate([
        contentGuide.widthAnchor.constraint(equalTo: frameGuide.widthAnchor)
      ])
    }
    
    NSLayoutConstraint.activate([
      background.trailingAnchor.constraint(equalTo: sheet.trailingAnchor),
      background.leadingAnchor.constraint(equalTo: sheet.leadingAnchor),
      background.topAnchor.constraint(equalTo: sheet.topAnchor),
      background.bottomAnchor.constraint(equalTo: sheet.bottomAnchor),
      
      header.trailingAnchor.constraint(equalTo: sheet.trailingAnchor),
      header.leadingAnchor.constraint(equalTo: sheet.leadingAnchor),
      header.topAnchor.constraint(equalTo: sheet.topAnchor),
      header.heightAnchor.constraint(equalToConstant: 75),
      
      labelsContainer.leadingAnchor.constraint(equalTo: header.leadingAnchor),
      labelsContainer.centerYAnchor.constraint(equalTo: header.centerYAnchor),
  
      titleLabel.centerXAnchor.constraint(equalTo: labelsContainer.centerXAnchor, constant: 22),
      titleLabel.topAnchor.constraint(equalTo: labelsContainer.topAnchor),
      
      subTitleLabel.centerXAnchor.constraint(equalTo: labelsContainer.centerXAnchor, constant: 22),
      subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: titleGap),
      subTitleLabel.bottomAnchor.constraint(equalTo: labelsContainer.bottomAnchor),

      contentGuide.leadingAnchor.constraint(equalTo: contentContainer.leadingAnchor),
      contentGuide.topAnchor.constraint(equalTo: contentContainer.topAnchor),
      contentGuide.trailingAnchor.constraint(equalTo: contentContainer.trailingAnchor),
      contentGuide.bottomAnchor.constraint(equalTo: contentContainer.bottomAnchor),

      scrollView.topAnchor.constraint(equalTo: header.bottomAnchor),
      scrollView.leadingAnchor.constraint(equalTo: sheet.leadingAnchor),
      scrollView.trailingAnchor.constraint(equalTo: sheet.trailingAnchor),

      contentContainer.leadingAnchor.constraint(equalTo: contentGuide.leadingAnchor),
      contentContainer.topAnchor.constraint(equalTo: contentGuide.topAnchor),
      contentContainer.trailingAnchor.constraint(equalTo: contentGuide.trailingAnchor),
      contentContainer.bottomAnchor.constraint(equalTo: contentGuide.bottomAnchor)
    ])
    
    //The ScrollView needs to update it's bottom constraint according to it's state
    scrollViewBottomConstraint = scrollView.bottomAnchor.constraint(equalTo: sheet.bottomAnchor, constant: -locationCollapsed + locationExtended)
    scrollViewBottomConstraint.isActive = true
  }
  
  ///If a dismiss button if it was supplied in the initializer, one will be added in this method
  private func configureDismissButtonIfNeeded() {
    if let _ = dismissButtonTapCompletion {
      dismissButton = UIButton(type: .system)
      guard let dismissButton = dismissButton  else { return }
      let origImage = UIImage(systemName: "xmark")
      let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
      dismissButton.setImage(tintedImage, for: .normal)
      dismissButton.tintColor = .black
      dismissButton.addTarget(self, action: #selector(BottomSheet.dismissButtonTapped), for: .touchUpInside)
      dismissButton.translatesAutoresizingMaskIntoConstraints = false
      header.addSubview(dismissButton)

      NSLayoutConstraint.activate([
        dismissButton.trailingAnchor.constraint(equalTo: header.trailingAnchor, constant: -20),
        dismissButton.centerYAnchor.constraint(equalTo: header.centerYAnchor),
        dismissButton.widthAnchor.constraint(equalToConstant: 44),
        dismissButton.heightAnchor.constraint(equalToConstant: 44),
        labelsContainer.trailingAnchor.constraint(equalTo: dismissButton.leadingAnchor),
      ])
    } else {
      NSLayoutConstraint.activate([
        labelsContainer.trailingAnchor.constraint(equalTo: header.trailingAnchor)
      ])
    }
  }
  
  /**
      When the pan guesture ends, a new sheet frame is configured

       - Parameters:
          - velocityY: The Y velocity used during the pan gesture
          - sheet: The single main sheet used to contain all the BottomSheet UI
  */
  private func configureBottomSheetFrame(velocityY: CGFloat, sheet: UIView) -> CGRect {
    var newFrame = sheet.frame

    //If there is a high velocity during an ending pan gesture, depending on the location of the sheet, animate to it's new location
    if velocityY <= -500 {
      //Pan gesture with high velocity going up
      if sheet.frame.origin.y > (locationExtended + locationCollapsed) / 2 || sheet.frame.origin.y < (locationExtended + locationCollapsed) / 2 {
        //Go from collapsed to extended
        newFrame.origin.y = locationExtended
        state = .extended
        delegate?.currentState(state)
      }
    } else if velocityY >= 500 {
      //Pan gesture with high velocity going down
      if sheet.frame.origin.y > (locationExtended + locationCollapsed) / 2 {
        newFrame.origin.y = locationDismissed
      } else if sheet.frame.origin.y < (locationExtended + locationCollapsed) / 2 {
        if velocityY >= 2000 {
          //With a very strong swipe down, immediately dismiss the sheet
          newFrame.origin.y = locationDismissed
        } else {
          //Go from extended to collapsed
          newFrame.origin.y = locationCollapsed
          state = .collapsed
          delegate?.currentState(state)
        }
      }
    } else {
      //If velocity is low and a let up on the pan gesture is soft, just animate to the required state
      //Use thresholds that equal thirds, of the size of the overall height of the BottomSheet
      if sheet.frame.origin.y < (locationExtended + locationCollapsed) / 2 {
        newFrame.origin.y = locationExtended
        state = .extended
        delegate?.currentState(state)
        //Extended
      } else if sheet.frame.origin.y > (locationDismissed + locationCollapsed) / 2 {
        //Dismissed
        newFrame.origin.y = locationDismissed
      } else {
        newFrame.origin.y = locationCollapsed
        //Collapsed
        state = .collapsed
        delegate?.currentState(state)
      }
    }
    
    return newFrame
  }
  
  // MARK: - Objective-C Runtime Methods
  
  /**
      Method is called when the dismiss button is tapped

       - Parameters:
          - sender: The dismiss button
  */
  @objc func dismissButtonTapped(sender: UIButton) {
    guard let dismissButtonTapCompletion = dismissButtonTapCompletion else { return }
    dismissButtonTapCompletion()
  }
  
  /**
      Method that is called during the pan gesture of the BottomSheet

       - Parameters:
          - gesture: The pan gesture
  */
  @objc func pan(_ gesture: UIPanGestureRecognizer) {
    guard let sheet = sheet else { return }
    let translation = gesture.translation(in: sheet)
    let velocityY = gesture.velocity(in: sheet).y
    if gesture.state == .began {
      //Stop the animator if an animation is being run while a pan gesture starts
      animator.stopAnimation(true)
      //It was decided to use frames as the main container of the BottomSheet.  This allowed for the easier interuption during a
      //pan gesture of the BottomSheet if an animation was taking place.
      //Animating constraints with an interrupting pan gesture while the UIView.animate is happening is pretty difficult.
      //If the animation was always being allowed to complete and not be
      //interrupted, animating constraints could be a better alternative.  However, everything else uses constraints for the subviews of the BottomSheet.
      sheetLastLocation = sheet.center
      sheet.center.y = sheetLastLocation.y + translation.y
    } else if gesture.state == .changed {
      sheet.center.y = sheetLastLocation.y + translation.y
      scrollViewBottomConstraint.constant = 0
    } else if gesture.state == .ended {
      sheet.center.y = sheetLastLocation.y + translation.y
      
      let newFrame = configureBottomSheetFrame(velocityY: velocityY, sheet: sheet)
      
      scrollViewBottomConstraint.constant = scrollViewBottomConstraintConstant
      
      //If we are the dismissed state, just run the dismiss button method that is called when the button is tapped
      if newFrame.origin.y == locationDismissed {
        guard let dismissButtonTapCompletion = dismissButtonTapCompletion else { return }
        dismissButtonTapCompletion()
      } else {
        let duration = velocityY <= -500 || velocityY >= 500 ? 0.45 : 0.75
        animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.5) {
          sheet.frame = newFrame
        }
        animator.startAnimation()
      }
    }
  }
  
  ///Detect orientation changes and recalculate the BottomSheet to display properly
  @objc func orientationChanged() {
    guard let sheet = sheet, let topWindow = UIApplication.shared.windows.first!.rootViewController?.view else { return }
    calculateSheetLocations(addedTo: topWindow)
    let sheetWidth = min(topWindow.frame.width, maximumSheetWidth)
    let sheetLocationX = topWindow.frame.width / 2 - sheetWidth / 2
    let resetLocationY = state == .extended ? locationExtended : locationCollapsed
    sheet.frame = CGRect(x: sheetLocationX, y: resetLocationY, width: sheetWidth, height: sheetHeight)
    scrollViewBottomConstraint.constant = scrollViewBottomConstraintConstant
    background.setNeedsDisplay()
  }
  
}

/// A background view contains gradients drawn through a CAGradientLayer.
private class BackgroundView: UIView {
  
  /// The first customizable color of the gradient
  var colorOne = UIColor.red
  /// The second customizable color of the gradient
  var colorTwo = UIColor.blue
  /// The gradient applied to the entire background
  private let gradient = CAGradientLayer()
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: rect.width, height: rect.height))
    
    let shape = CAShapeLayer()
    shape.frame = frame
    shape.path = path.cgPath
    
    gradient.frame = frame
    gradient.colors = [colorOne.cgColor, colorTwo.cgColor]
    gradient.startPoint = CGPoint(x: 0.5, y: 0)
    gradient.endPoint = CGPoint(x: 0.5, y: 1)
    gradient.mask = shape

    layer.addSublayer(gradient)
  }
}

extension UIView {
  /**
   Apply any combination of rounded corners to a view

   - Parameters:
          - corners: The combination of corners supplied
          - radius: The value supploed to the cornerRadius
  */
  func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
    self.layer.maskedCorners = corners
    self.layer.cornerRadius = radius
  }
}
